<?php
/**
 * Created by PhpStorm.
 * User: HONG
 * Date: 8/6/14
 * Time: 5:08 PM
 */ ?>
<div id="back_top" style="display: block;" title="Top page"></div>
<script type="text/javascript">
    jQuery(function() {
        jQuery(window).scroll(function() {
            if(jQuery(this).scrollTop() > 300) {
                jQuery('#back_top').fadeIn();
            } else {
                jQuery('#back_top').fadeOut();
            }
        });
        jQuery('#back_top').click(function() {
            jQuery('body,html').animate({scrollTop:0},500);
        });
    });
</script>