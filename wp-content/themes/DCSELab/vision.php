<?php
/**
 * Created by PhpStorm.
 * User: HONG
 * Date: 8/6/14
 * Time: 10:23 AM
 */ ?>
<div class="vision">
    <?php
    $post = get_post(96);
    $content = apply_filters('the_content', $post->post_content);
    $contents=$content;
    $content= wp_trim_words($contents,50,' ...') ;?>
    <h3 class="h3"><?php echo the_title()?></h3>
    <a href="<?php echo get_page_link(96); ?>">
    <?php echo the_post_thumbnail(96);?>
    </a>
    <div class="content">
        <?php
        echo  $content;?>
    </div>

</div>