<?php
/**
 * Created by PhpStorm.
 * User: HONG
 * Date: 7/31/14
 * Time: 12:57 PM
 */


function dcselab_setup(){

    /***add Enable support for Post Thumbnails, and declare two sizes.**/

    add_theme_support('post-thumbnails');
    set_post_thumbnail_size( 672, 372, true );
    add_image_size( 'dcselab-theme-full-width', 1038, 576, true );

    /**dang ky menu**/
    register_nav_menus( array(
        'primary'   => __( 'Top primary menu', 'dcselab' ),
        'secondary' => __( 'Secondary menu in left sidebar', 'dcselab' ),
    ) );
    /****/
    if ( ! is_admin() ) {
// Hook in early to modify the menu
// This is before the CSS "selected" classes are calculated
        add_filter( 'wp_get_nav_menu_items', 'display_lasts_ten_posts_for_categories_menu_item', 100, 3 );
    }

// Add the ten last posts of af categroy menu item in a sub menu
    function display_lasts_ten_posts_for_categories_menu_item( $items, $menu, $args ) {


        $menu_order = count($items); /* Offset menu order */
        $child_items = array();

// Loop through the menu items looking category menu object
        foreach ( $items as $item ) {

            // Test if menu item is a categroy and has no sub-category
            if ( 'category' != $item->object || ('category' == $item->object && get_category_children($item->object_id)) )
                continue;

            // Query the lasts ten category posts
            $category_ten_last_posts = array(
                'numberposts' => 100,
                'cat' => $item->object_id,
                'orderby' => 'date',
                'order' => 'DESC'
            );

            foreach ( get_posts( $category_ten_last_posts ) as $post ) {
                // Add sub menu item
                $post->menu_item_parent = $item->ID;
                $post->post_type = 'nav_menu_item';
                $post->object = 'custom';
                $post->type = 'custom';
                $post->menu_order = ++$menu_order;
                $post->title = $post->post_title;
                $post->url = get_permalink( $post->ID );
                /* add children */
                $child_items[]= $post;
            }
        }
        return array_merge( $items, $child_items );
    }
    /**Them class first and last vao trong the li cua menu**/
    function add_first_and_last($output) {
        $output = preg_replace('/class="menu-item/', 'class="first menu-item', $output, 1);
        $output = substr_replace($output, 'class="last menu-item', strripos($output, 'class="menu-item'), strlen('class="menu-item'));
        return $output;
    }
    add_filter('wp_nav_menu', 'add_first_and_last');
    /**end**/

    /**ham tim ki tu dac biet trong chuoi va them vao ki tu moi torng chuoi**/
    function add_character_special($string){
        $string_add_br=strstr($string,"-","<br>");
        return $string_add_br;
    }
    function m_explode(array $array, $key = '')
    {
        if (!is_array($array) or $key == '')
            return;
        $output = array();
        foreach ($array as $v) {
            if (!is_object($v)) {
                return;
            }
            $output[] = $v->$key;
        }
        return $output;
    }

if ( function_exists('register_sidebar') )
    register_sidebar();

/**end**/
    //BEGIN thiet lap so luong truy cap bai viet
    function setPostViews($postID) {
        $count_key = 'post_views_count';
        $count = get_post_meta($postID, $count_key, true);
        if($count==''){
            $count = 0;
            delete_post_meta($postID, $count_key);
            add_post_meta($postID, $count_key, '0');
        }else{
            $count++;
            update_post_meta($postID, $count_key, $count);
        }
    }
//BEGIN lay so luong truy cap bai viet
    function getPostViews($postID){
        $count_key = 'post_views_count';
        $count = get_post_meta($postID, $count_key, true);
        if($count==''){
            delete_post_meta($postID, $count_key);
            add_post_meta($postID, $count_key, '0');
            return "0 View";
        }
        return $count;
    }
//end


}
add_action('after_setup_theme','dcselab_setup');