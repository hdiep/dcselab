<?php
/**
 * Created by PhpStorm.
 * User: HONG
 * Date: 7/30/14
 * Time: 9:34 AM
 */
get_header()?>
<?php include('slide.php')?>
    </header><!-- #masthead -->

<div id="main" class="site-main"> <!--end main-->
    <div class="main-content">
        <div class="wrapper">
            <div class="column1">
                <?php include('about-us.php'); ?>
                <?php include('investigate.php'); ?>
                <?php include('cgcn-dv.php'); ?>
                <?php include('top-video.php');?>

            </div>
            <div class="column2">
                <?php include('external-relations.php'); ?>
                <?php include('find-us-facebook.php'); ?>
            </div>
            <div class="column3">
                <?php include('education.php'); ?>
                <?php include('news.php'); ?>
            </div>
            <div class="top-footers">
                <?php include('vision.php'); ?>
                <?php include('center-values.php'); ?>
                <?php include('contact.php'); ?>
            </div>
        </div>
    </div>
<?php get_footer() ?>