<?php
/**
 * Created by PhpStorm.
 * User: HONG
 * Date: 8/7/14
 * Time: 9:09 AM
 */ ?>
 <?php get_header()?>
   </header><!-- #masthead -->

 <div id="main" class="site-main">
   <div class="page-contact-us">
       <div class="content-page">
           <div class="contents">
             <?php	while ( have_posts() ) : the_post();

                // Include the page content template.
                   echo the_content();
                 // If comments are open or we have at least one comment, load up the comment template.
                    endwhile;
             ?>
         </div>
         </div>
          <?php include('education.php')?>
 <?php get_footer();?>
