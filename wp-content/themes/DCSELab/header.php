<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial=1.0">
    <title><?php wp_title('', true, 'right'); ?></title>
    <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" type="image/x-icon"/>
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <link href="<?php echo get_stylesheet_uri(); ?>" rel="stylesheet" type="text/css" media="screen">
    <!--[if lt IE 9]>
    <script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
    <![endif]-->
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
    <?php if (get_header_image()) : ?>
        <div id="site-header">
            <a href="<?php echo esc_url(home_url('/')); ?>" rel="home">
                <img src="<?php header_image(); ?>" width="<?php echo get_custom_header()->width; ?>"
                     height="<?php echo get_custom_header()->height; ?>" alt="images header" title="DCSELab"/>
            </a>
        </div>
    <?php endif; ?>
    <header id="masthead" class="site-header" role="banner">
        <div class="bg-banners">
            <div class="header-main">
                <div class="round-header-logo-text">
                    <a href="<?php echo esc_url(home_url('/')); ?>" rel="home">
                        <div class="logo"><img src="<?php echo get_template_directory_uri(); ?>/images/logo.png"
                                               alt="Logo" title="Logo"/></div>
                    </a>

                    <div class="text-logo">
                        <img src="<?php echo get_template_directory_uri();?>/images/bnPTN.png" title="Phòng thí nghiệm" alt="Logo"/>
                    </div>
                    <div class="logo-language">
                        <div class="logo-bk">
                            <a href="<?php echo esc_url(home_url('/')); ?>" rel="home">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/logo_bk.png"
                                     alt="Logo bk" title="Logo BK"/>
                            </a>
                        </div>
                        <div class="languages">
                            <!--                        --><?php //qtrans_generateLanguageSelectCode('image'); ?>
                        </div>
                    </div>
                </div>
                <nav id="primary-navigation" class="site-navigation primary-navigation" role="navigation">
                    <a href="<?php echo esc_url(home_url('/')); ?>"><img
                            src="<?php echo get_template_directory_uri() ?>/images/icon-home.png" class="home-page"
                            alt="Icon home"/></a>
                    <?php wp_nav_menu(array('theme_location' => 'primary', 'menu_class' => 'nav-menu')); ?>
                </nav>
            </div>
        </div>

