<?php
/**
 * Created by PhpStorm.
 * User: HONG
 * Date: 8/7/14
 * Time: 2:09 PM
 */
?>
<div class="external-relations videos">
    <div class="titles"><h3 class="h3-font">VIDEO</h3></div>
    <div class="content">
        <ul>
            <?php
            $arg_video = array('category_name' => 'videos', 'showposts' => 2, 'orderby' => 'rand');
            $query_video = new WP_Query($arg_video);
            if ($query_video->have_posts())
                while ($query_video->have_posts()): $query_video->the_post();?>
                    <li>
                        <h4 class="h4"><a href="<?php echo the_permalink(); ?>"> <?php the_title();?></a></h4>
                        <?php echo the_content(); ?>
                    </li>
                <?php endwhile ?>
        </ul>
    </div>
</div>