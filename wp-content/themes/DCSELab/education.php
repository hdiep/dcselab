<?php
/**
 * Created by PhpStorm.
 * User: HONG
 * Date: 8/4/14
 * Time: 1:16 PM
 */ ?>
<div class="education">
    <div class="title"></div>
    <div class="content">
        <ul>
            <?php
            global $post;
            $i=0;
            $array = array('category_name' => 'education', 'orderrand' => 'rand', 'numberposts' => '2');
            $result = get_posts($array);
            foreach ($result as $post): setup_postdata($post);
                $i=$i++;
                ?>
                <li class="<?php if($i%2==0){echo "even";}else{echo "odd";}?>">
                    <a href="<?php echo the_permalink();?>" title="1-3">
                        <?php echo the_post_thumbnail();?>
                    </a>
                    <h4 class="h4"> <a href="<?php echo the_permalink()?>"> <?php echo the_title();?></a></h4>
                    <p><?php echo $content= wp_trim_words(get_the_content(),26,'...' ) ;?></p>
                </li>
                <?php $i=$i+1;?>
            <?php endforeach ?>
        </ul>
    </div>
</div>