<?php
/**
 * Created by PhpStorm.
 * User: HONG
 * Date: 4/9/14
 * Time: 2:35 PM
 */
get_header();
?>
    </header><!-- #masthead -->
<div id="main" class="site-main">
    <div class="page-contact-us">
        <div class="content-page">
            <div class="contents single">
                <?php if (have_posts()) : ?>
                    <?php while (have_posts()) : the_post(); ?>
                        <?php setPostViews(get_the_ID()); ?>
                        <div class="box-title">
                            <h3> <?php echo the_title() ?></h3>
                            <div class="infor">
                                <i> - Danh mục:  <?php the_category(','); ?>
                                    - Số lượng xem: <?php echo getPostViews(get_the_ID()); ?>
                                    - Ngày đăng : <?php echo the_date('d/m/Y'); ?>
                                </i>
                            </div>
                            <div class="content">
                                <?php echo the_content(); ?>
                            </div>

                            <!---hien thi bai viet lien quan-->
                            <div class="relatived-post">
                                    <div class="my-recent-posts">
                                        <h3 class="h3">BÀI VIẾT LIÊN QUAN:</h3>
                                        <ul class="related-post">
                                            <?php
                                            //for use in the loop, list 5 post titles related to first tag on current post
                                            global $post;
                                            $current_post = $post->ID;
                                            $categories = get_the_category();
                                            foreach ($categories as $category) : ?>
                                            <?php
                                            $posts = get_posts('showposts=8&offset=0&orderby=rand&category=' . $category->term_id . '&exclude=' . $current_post);
                                            foreach ($posts as $post) :
                                                ?>
                                                <li><a href="<?php the_permalink(); ?>"
                                                       title="Xem"><?php the_title(); ?></a>
                                                </li>
                                            <?php endforeach; ?>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                            </div>
                            <!---end hien thi bai viet lien quan--->
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="left-column">
        <?php include('news2.php');?>
        <?php include('video.php');?>
        </div>
    </div>
<?php get_footer() ?>