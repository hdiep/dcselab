<?php
/**
 * Created by PhpStorm.
 * User: HONG
 * Date: 8/26/14
 * Time: 10:37 AM
 */
?>
<div class="about-us top-video">
    <?php
    $arg_video = array('category_name' => 'videos', 'showposts' => 1, 'orderby' => 'rand');
    $query_video = new WP_Query($arg_video);
    if ($query_video->have_posts())
        while ($query_video->have_posts()): $query_video->the_post();?>
            <h4 class="h4"><a href="<?php echo the_permalink(); ?>"> <?php the_title(); ?></a></h4>
            <div class="content"> <?php echo the_content(); ?></div>
        <?php endwhile ?>
</div>
