<?php
/**
 * Created by PhpStorm.
 * User: HONG
 * Date: 8/6/14
 * Time: 10:13 AM
 */
?>
<div class="contact">

    <?php
    $post = get_post(8);
    $content = apply_filters('the_content', $post->post_content);

 ?>
    <h3 class="h3"><?php echo the_title() ?></h3>

    <a href="<?php echo get_page_link(8); ?>">
        <?php echo the_post_thumbnail(8); ?>
    </a>

    <div class="content">
        <?php
        echo $content;?>
    </div>
</div>