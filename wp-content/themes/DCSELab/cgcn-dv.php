<?php
/**
 * Created by PhpStorm.
 * User: HONG
 * Date: 8/4/14
 * Time: 1:13 PM
 */
?>
<script src="<?php echo get_template_directory_uri(); ?>/carouselengine/jquery.js"></script>
<script type="application/javascript">$.noConflict();</script>
<link href="<?php echo get_stylesheet_directory_uri();?>/carouselengine/initcarousel-1.css" rel="stylesheet" type="text/css" media="screen">
<script src="<?php echo get_template_directory_uri(); ?>/carouselengine/amazingcarousel.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/carouselengine/initcarousel-1.js"></script>
<div class="cgcn-dv">
        <div id="amazingcarousel-1"
             style="display:block;position:relative;width:100%;max-width:457px;margin:0px auto 0px;overflow: hidden">
            <div class="amazingcarousel-list-container" style="overflow:hidden;">
                <ul class="amazingcarousel-list">
                    <?php
                    global $post;
                    $array = array('category_name' => 'cgcn-dv', 'orderrand' => 'rand', 'numberposts' => '5');
                    $result = get_posts($array);
                    foreach ($result as $post): setup_postdata($post);
                    ?>
                    <li class="amazingcarousel-item">
                        <div class="amazingcarousel-item-container">
                            <div class="amazingcarousel-image">
                                <a href="<?php the_permalink();?>" title="1-3" class="html5lightbox"
                                   data-group="amazingcarousel-1">
                                    <?php the_post_thumbnail();?>
                                </a>
                            </div>
                            <div class="amazingcarousel-text">
                                <h3 class="h3"><?php echo the_title();?></h3>
                                <div class="amazingcarousel-description"> <?php echo $content= wp_trim_words(get_the_content(),78,'' ) ;?></div>
                            </div>
                        </div>
                    </li>
                    <?php endforeach ?>
                </ul>
            </div>
            <div class="amazingcarousel-prev"></div>
            <div class="amazingcarousel-next"></div>
            <div class="amazingcarousel-nav"></div>
        </div>
</div>