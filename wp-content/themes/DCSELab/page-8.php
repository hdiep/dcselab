<?php
/**
 * Created by PhpStorm.
 * User: HONG
 * Date: 8/6/14
 * Time: 4:06 PM
 */ ?>
<?php get_header();?>
    </header><!-- #masthead -->
<div id="main" class="site-main"><!--end main page-->
<div class="page-contact-us">
    <div class="content-page">
        <div class="title">
            <?php $page_contact = get_page(8);
            $content = apply_filters('the_content', $page_contact->post_content);
            echo $content;
            ?>
        </div>
        <?php echo do_shortcode("[contact-form-7 id='49' title='Liên hệ']");?>
    </div>
    <?php include('education.php')?>
</div>
<?php get_footer();?>
