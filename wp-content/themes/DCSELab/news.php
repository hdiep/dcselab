<?php
/**
 * Created by PhpStorm.
 * User: HONG
 * Date: 8/4/14
 * Time: 1:15 PM
 */
?>
<div class="news">
    <div class="title"></div>
    <ul class="content">
        <?php
        global $post;
        $i=1;
        $array = array('category_name' => 'news', 'orderrand' => 'desc', 'numberposts' => '5');
        $result = get_posts($array);
        foreach ($result as $post): setup_postdata($post);
        $i=$i++;
        ?>
        <li class="item-<?php if($i%3==0){echo $i;}else{echo $i;}?>">
            <h4 class="h4"> <a href="<?php echo the_permalink()?>"> <?php echo the_title();?></a></h4>
            <a href="<?php echo the_permalink(); ?>">
                <?php echo get_the_post_thumbnail();?>
            </a>

            <div class="details"><?php echo $content= wp_trim_words(get_the_content(),16,'...' ) ;?></div>
        </li>
            <?php $i=$i+1;?>
        <?php endforeach ?>
    </ul>
</div>
