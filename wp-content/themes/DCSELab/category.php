<?php
/**
 * Created by PhpStorm.
 * User: HONG
 * Date: 8/7/14
 * Time: 4:05 PM
 */
?>
<?php get_header() ?>
    </header><!-- #masthead -->

<div id="main" class="site-main"><!--end main page-->
    <div class="page-contact-us categories">
        <ul class="category content-page show-categories">
            <?php
            $object = $wp_query->get_queried_object();

            // Get parents of current category
            $parent_id = $object->category_parent;
            $cat_breadcrumbs = '';
            while ($parent_id) {
                $category = get_category($parent_id);
                $cat_breadcrumbs = '<a href="' . get_category_link($category->cat_ID) . '">' . $category->cat_name . '</a> &raquo; ' . $cat_breadcrumbs;
                $parent_id = $category->category_parent;
            }
            $result = $cat_breadcrumbs . $object->cat_name;
            ?>
            <h3><a href="<?php bloginfo('home'); ?>"><?php _e('Home'); ?></a> &raquo; <?php echo $result; ?>
            </h3>
            <?php while (have_posts()) :
                the_post(); ?>
                <li class="category-item clearfix" id="post-<?php the_ID(); ?>">
                    <h4 class="h4"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title_attribute(); ?></a></h4>

                    <div class="post-info">
                        <span class="date">Ngày đăng: <?php the_time(__('d/m/Y')) ?></span> | <!--ngay dang bai viet-->
                        <?php if (function_exists('the_views')) { ?>
                            <span class="views"><?php the_views(); ?></span> | <!--so luot nguoi xem bai viet-->
                        <?php } ?>

                        <?php edit_post_link(__('Sửa bài viết'), '', ''); ?><!--Edit bai-->

                    </div>
                    <a href="<?php echo the_permalink();?>">
                    <?php echo the_post_thumbnail('thumbnail'); ?>
                    </a>
                    <div class="content clearfix"><?php the_excerpt(); ?></div>
                    <!--show noi dung ngan-->
                </li>
            <?php endwhile; ?>
        </ul>
        <div class="left-column">
            <?php include('video.php');?>
            <?php include('find-us-facebook2.php');?>
        </div>

    </div>
<?php get_footer() ?>