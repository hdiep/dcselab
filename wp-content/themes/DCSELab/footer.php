<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>
<div class="site-footer">
    <?php include('partner.php')?>
    <nav id="primary-navigation" class="site-navigation primary-navigation" role="navigation">
        <a href="<?php echo esc_url(home_url('/'));?>"><img src="<?php echo get_template_directory_uri()?>/images/icon-home.png" class="home-page" alt="icon-home"/></a>
        <?php wp_nav_menu( array( 'menu' => 'menu-bottom' )); ?>
    </nav>
</div>
</div><!-- #main -->
<footer id="colophon" class="site-bottom " role="contentinfo">
    <div class="site-info">
        <div class="address">
            <h3>PHÒNG THÍ NGHIỆM TRỌNG ĐIỂM QUỐC GIA ĐIỀU KHIỂN SỐ VÀ KÝ THUẬT HỆ THỐNG</h3>

            <p> Nhà C6, 268 Lý Thường Kiệt, Q.10, T.P Hồ Chí Minh
                <br>Bản quyền @ 2008 thuộc về Phòng thí nghiệm trọng điểm quốc gia điều khiển số và kxỹ thuật hệ thống
            </p>
        </div>
        <div class="social">
            <?php echo get_sidebar(); ?>
            <?php echo ctsocial_icons_template(); ?></div>
    </div>
    <!-- .site-info -->
</footer><!-- #colophon -->
</div><!-- #page -->
<?php wp_footer(); ?>
</body>
</html>